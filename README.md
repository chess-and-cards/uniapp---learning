# COOL-UNI
 
 


## 技术栈

| 技术       | 说明             | 官网                                                                             |
|------------|------------------|----------------------------------------------------------------------------------|
| Vite       | 脚手架           | [https://vitejs.cn//](https://vitejs.cn//)                                         |
| Vue        | 前端框架         | [https://vuejs.org/](https://vuejs.org/)                                         |
| pinia      | 状态管理         | [https://pinia.vuejs.org/zh/](https://pinia.vuejs.org/zh/)                    |                         
| TypeScript | 语言            | [https://www.typescriptlang.org/](https://www.typescriptlang.org)                  |
| Axios      | 前端HTTP框架     | [https://github.com/axios/axios](https://github.com/axios/axios)                 |
| vk-uview   |  前端UI框架      | [https://vkuviewdoc.fsq.pub/](https://vkuviewdoc.fsq.pub/)                           | 
| uniapp     | uniapp          | [https://uniapp.dcloud.net.cn/](https://uniapp.dcloud.net.cn/)                       |
| tailwind   | tailwind        | [https://tailwind.nodejs.cn/](https://tailwind.nodejs.cn/)                       |